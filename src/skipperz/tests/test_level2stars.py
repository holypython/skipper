import os
folder = os.path.dirname(__file__)
folder, tail = os.path.split(folder)
os.chdir(folder)
print ("test_level1_scrol is running in this directory: ", folder)

from ..levels.level_2 import Level2
import pygame

import zzChrono.zz_chrono as zz_chrono

def init():
    global screen, clock, level

    pygame.init()
    screen = pygame.display.set_mode((1024, 668),pygame.DOUBLEBUF)
    clock = pygame.time.Clock()

    level = Level2(screen.get_rect())
    level.start()

def refresh():
    clock.tick(30)
    pygame.display.flip()
    pygame.event.pump()

init()
""" 
for _ in range(150): # a 30 tick/s, ca fait 5 secondes
    # just test the scrolling
    refresh()

    level.update()
    level.draw_background(screen)



for _ in range(150): # a 30 tick/s, ca fait 5 secondes
    # just test display the baddies
    refresh()

    level.update()
    screen.fill((0, 0, 0, 0))
    level.drawBaddies(screen)

"""

while True:
    # just test display the baddies
    refresh()
    level.draw(screen)

    if not level.update():
        zz_chrono.display_all()
        exit()
