sprites
===
spaceship-2135750_1280.png
----
source: https://pixabay.com/fr/illustrations/vaisseau-spatial-ovni-3d-rendre-2135750/

licence: Pixabay License
Libre pour usage commercial
Pas d'attribution requise

rocket-306209_1280.webp
----
source: https://pixabay.com/fr/vectors/fus%c3%a9e-espace-vaisseau-spatial-306209/
Pixabay License
Libre pour usage commercial
Pas d'attribution requise


sounds
======
drone.mp3 by hykenfreak
----
https://freesound.org/people/hykenfreak/sounds/214663/


alien.mod
by Feekzoid 
http://www.ankman.de/amiga-mod-music/

malabar.mod
by moby

condom corruption.mod
by spaceballs