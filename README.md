Skipperz, by Simon Zozol
=======
![screenshot from skipperz](https://holy-python.com/skipperz/skipperz_screenshot.jpg)


Description
----
In this game, you control a spaceship initially at the right side of the screen.

Some other ships will come from the left

Just skip them

install and run
-----
Easiest way to install is from you system console:
	pip install skipperz

then run with this command:
	python -m skipperz


controls
----
Curently, you can only use the keyboard directional arrows.

Well... actually, you can try to edit `params.py` directly on your drive. It is somewhat technical, but nothing crazy.

I will add other methods later (touchscreen, mouse, joypad)


licence
----
Licence Creative common 0. 

You do whatever you want with this. I take no responsability if it backfires.


Where can I follow the wonderful work of Simon Zozol?
-----
holy-python.com

It might be in French

 